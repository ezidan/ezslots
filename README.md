# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* EZ Games Slot Machines game (ezslots)
* 1.0-SNAPSHOT

### How do I get set up? ###

1. Clone the repository
2. Run the following command from the project root directory
```
#!sh

./gradlew build
```

### Contribution guidelines ###

* Writing tests - write tests!
* Code review
* Other guidelines

### Who do I talk to? ###

* Idan Avisar [idan@ezgames.tech](mailto:idan@ezgames.tech)
* Vadim Drabkin [vadim@ezgames.tech](mailto:vadim@ezgames.tech)